﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCP_Examen_1_Tanque_de_Agua
{
    public partial class frmPrincipal : Form
    {
        // Espacio para declaración de variables globales dentro del formulario


        // Fin del Espacio para declaración de variables globales dentro del formulario

        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = true;
            tmrVaciar.Enabled = false;
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = false;
            tmrVaciar.Enabled = true;
        }

        private void btnParo_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = false;
            tmrVaciar.Enabled = false;
        }

        /*
            *Operaciones ariteméticas:
            *Suma:       A = B + C;     incremento en 1:            A++;
            *Resta:      A = B - C;     decremento en 1:            A--;
            *Producto:   A = B * C;     incremento en un valor X:   A += 5;  //equivalente a:    A = A + 5;
            *Cociente:   A = B / C;     decremento en un valor X:   A -= 5;  //equivalente a:    A = A - 5;
            *Residuo:    A = B % C;
         */

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (panelAgua.Height < panelTanque.Height)
            {
                double InctrementoPorciento = panelTanque.Height * 0.025;
                int IncrementoEntero = (int)Math.Round(InctrementoPorciento, 0);
                panelAgua.Height += IncrementoEntero;
            }
            else
            {
                tmrLlenar.Enabled = false;
            }
        }

        private void tmrVaciar_Tick(object sender, EventArgs e)
        {
            if (panelAgua.Height > 0)
            {
                double DecrementoPorciento = panelTanque.Height * 0.025;
                int DecrementoEntero = (int)Math.Round(DecrementoPorciento, 0);
                panelAgua.Height -= DecrementoEntero;
            }
            else
            {
                tmrVaciar.Enabled = false;
            }
        }

        private void timerActuadores_Tick(object sender, EventArgs e)
        {
            if (tmrLlenar.Enabled)
            {
                pbxLlenar.BackColor = Color.GreenYellow;
            }
            else
            {
                pbxLlenar.BackColor = Color.FromArgb(64,0,0);
            }


            if (tmrVaciar.Enabled)
            {
                pbxVaciar.BackColor = Color.GreenYellow;
            }
            else
            {
                pbxVaciar.BackColor = Color.FromArgb(64, 0, 0);
            }
        }
    }
}
